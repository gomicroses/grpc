module gitlab.com/gomicroses/grpc

go 1.17

require (
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/golang/protobuf v1.3.3 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0 // indirect
	github.com/mwitkow/go-proto-validators v0.3.2 // indirect
	golang.org/x/net v0.0.0-20201021035429-f5854403a974 // indirect
	golang.org/x/sys v0.0.0-20200930185726-fdedc70b468f // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/genproto v0.0.0-20200423170343-7949de9c1215 // indirect
	google.golang.org/grpc v1.29.1 // indirect
)
