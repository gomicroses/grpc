
## gen_proto: generate protobuf
gen_proto:
	protoc -I=. --go_out=./pkg/auth --go_opt=module=gitlab.com/gomicroses/grpc/pkg/auth   --go-grpc_out=./pkg/auth --go-grpc_opt=module=gitlab.com/gomicroses/grpc/pkg/auth   api/auth/auth.proto
	protoc -I=. --go_out=./pkg/mailer --go_opt=module=gitlab.com/gomicroses/grpc/pkg/mailer   --go-grpc_out=./pkg/mailer --go-grpc_opt=module=gitlab.com/gomicroses/grpc/pkg/mailer   api/mailer/mail.proto
	#protoc -I=../grpc --go_out=./pkg/auth --go_opt=paths=source_relative   --go-grpc_out=./pkg/auth --go-grpc_opt=path2s=source_relative   api/auth/login.proto
	# новые файлы будут складываться туда, какой пакет указан в login.proto
	#protoc -I=auth --go_out=auth/pkg --go_opt=paths=source_relative   --go-grpc_out=auth/pkg --go-grpc_opt=paths=source_relative    auth/api/security/login.proto
	#protoc --go_out=. --go_opt=paths=source_relative     --go-grpc_out=auth/pkg --go-grpc_opt=paths=source_relative     ./auth/api/security/login.proto

.PHONY: help
all: help
help: Makefile
	@sed -n 's/^##//p' $< | column -t -s ':' |  sed -e 's/^/ /'
	@echo

